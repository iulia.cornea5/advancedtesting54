package com.sda.advancedTesting.zoo;

import com.sda.advancedTesting.zoo.entity.Animal;
import com.sda.advancedTesting.zoo.repository.AnimalRepository;
import com.sda.advancedTesting.zoo.service.AnimalService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class AnimalServiceTest {

    @Mock
    private AnimalRepository animalRepository;

    @InjectMocks
    private AnimalService animalService;

    @Test
    void saveAnimalTest() {
        Animal animalToSave = new Animal(null, "Lion", "Tobby");
        Animal savedAnimal = new Animal(3, "Lion", "Tobby");
        when(animalRepository.save(animalToSave)).thenReturn(savedAnimal);

        Animal a = animalService.save(animalToSave);
        assertThat(a).isEqualTo(savedAnimal);
    }

    @Test
    void shouldThrowException() {
        Animal animalToSave = new Animal(null, "", "Tobby");

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(
                        () -> animalService.save(animalToSave)
                );
        verifyNoInteractions(animalRepository);
    }
}
