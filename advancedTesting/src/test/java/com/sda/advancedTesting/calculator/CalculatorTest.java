package com.sda.advancedTesting.calculator;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {

    @ParameterizedTest
    @ValueSource(strings = {"ala", "bala", "porto", ""})
    void verify_if_value_is_not_null(String val) {
        assertNotNull(val);
    }

    @ParameterizedTest
    @ValueSource(strings = {"ala", "bala", "porto"})
    void verify_if_value_is_not_empty(String val) {
        assertNotEquals("", val);
    }


}
