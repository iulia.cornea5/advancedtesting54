package com.sda.advancedTesting.zoo.service;

import com.sda.advancedTesting.zoo.entity.Animal;
import com.sda.advancedTesting.zoo.repository.AnimalRepository;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

@Service
public class AnimalService {

    private final AnimalRepository animalRepository;

    public AnimalService(AnimalRepository animalRepository) {
        this.animalRepository = animalRepository;
    }

    public Animal save(Animal a) {
        if(Strings.isBlank(a.getRace())) {
            throw new IllegalArgumentException("Animal race is a mandatory field");
        }
        Animal saved = animalRepository.save(a);
        return saved;
    }
}
