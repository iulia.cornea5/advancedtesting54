package com.sda.advancedTesting.calculator;

import com.sda.advancedTesting.calculator.exceptions.TruncatedResultException;

public class Calculator {

    public Double add(Double a, Double b) {
        return a + b;
    }

    public Double subtract(Double a, Double b) {
        return a - b;
    }

    //      a * b = ...  < Double.MAX_VALUE
    //      a * b < Double.MAX_VALUE
    //      a < Double.MAX_VALUE / b
    public Double multiply(Double a, Double b)  {
        if (a > Double.MAX_VALUE / b) {
            throw new TruncatedResultException("Cannot multiply numbers of this size. Result would be truncated.");
        }
        return a * b;
    }

    public Double divide(Double a, Double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Division by zero is not supported.");
        }
        return a / b;
    }

    public Integer modulo(Integer a, Integer b) {
        if (b == 0) {
            throw new IllegalArgumentException("Modulo by zero is not supported.");
        }
        return a % b;
    }

    // Example for method which throws unchecked exception
    public void uncheckedE() {
        throw new RuntimeException("This is an exception that does not need to be checked");
    }

    // Example for method which throws checked exception
    public void checkedE() throws Exception {
        throw new Exception("This is a checked exception");
    }
}
